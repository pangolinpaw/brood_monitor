# Brood Monitor

## Introduction

This software is designed to remotely monitor and log data on the state of a chicken brooder. The ambition is to eventually close the loop and have it control heating, ventilation etc. to maintain the optimum conditions for the brooder's ihabitants.

## Hardware requirements

- [PiSugar](https://github.com/PiSugar/PiSugar/wiki/PiSugar2) UPS
- 2 [temperature & humidity sensors](https://thepihut.com/products/sht30-temperature-and-humidity-sensor-wired-enclosed-shell)
- [Dimmer relay HAT](https://www.unmannedtechshop.co.uk/product/2-ch-triac-dimmer-hat/) connected to
- 2 [heat lamps](https://www.amazon.co.uk/Intelec-INFRA-poultry-brooder-livestock/dp/B07KCPS95N)

## Software setup

### Prerequisites

- Rasbperry pi running the latest version of Raspbian
- Python 3 and pip installed
- [PiSugar](https://github.com/PiSugar/PiSugar/wiki/PiSugar2) connected & installed


### Installation

1. Clone this repo with `git clone https://bitbucket.org/pangolinpaw/brood_monitor.git`
2. Navigate into the newly created directory with `cd brood_monitor`
3. Install requirements with `pip3 install -r requirements.txt`

## Usage

The scripts can be started manually, but it's better to have them run at boot with the help of the crontab (you may need to adjust the path depending on where you cloned the repo):

```
@reboot sleep 10 && nohup python3 /home/pi/brood_monitor/environment.py &      # temp/humidity logger
@reboot sleep 10 && nohup python3 /home/pi/brood_monitor/relay.py &            # scheduled lighting
@reboot sleep 20 && nohup python3 /home/pi/brood_monitor/camera.py &           # camera stream
@reboot sleep 30 && nohup python3 /home/pi/brood_monitor/brood_monitor.py &    # web interface
```

Once brood_monitor.py is running, the web interface will be accessible from any device on the same network as the pi and can be found at your pi's IP address on port 5000. e.g. if your pi has the IP 192.168.1.100, the URL to enter will be:

http://192.168.1.100:5000

