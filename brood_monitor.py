import os
import csv
import json
from datetime import datetime, timedelta
import platform
from flask import Flask, render_template, request, redirect, send_file, jsonify

import camera

if platform.system() == 'Windows':
    import dummy_relay as relay
else:
    import relay

relay.init()

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

# === Front end / user interface ===
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def root():
    if request.method == 'POST':
        if 'download' in request.form:
            return send_file(f'{THIS_DIRECTORY}data{os.sep}environment_log.csv', as_attachment=True)

        if 'heater' in request.form:
            if int(request.form['heater']) == 1:
                relay.on([8])
            else:
                relay.off([8])

        if 'relay' in request.form:
            relay.off(list(range(1, 9)))
            relay.on(list(range(1, int(request.form['relay']) +1)))

    relays = relay.state()
    for relay_number in relays:
        if int(relay_number) <= 6:
            if relays[relay_number] == 1:
                break # Find last relay that's ON
        else:
            break

    latest_readings = load_csv(f'{THIS_DIRECTORY}data{os.sep}environment_log.csv', 300)
    current = {
        'temperature': int(round(latest_readings[-1][1], 0)),
        'humidity': int(round(latest_readings[-1][2], 0))
    }
    recent = [[r[0].split(' ')[1], int(round(r[1], 0)), int(round(r[2], 0)), r[3], int(r[4])] for r in latest_readings[-9:]]
    recent.reverse()
    charts = environment_charts(latest_readings)
    charts = {
        'temperature': charts[0],
        'humidity': charts[1]
    }
    return render_template(
        'index.html',
        current = current,
        latest_readings = recent,
        chart = charts,
        max_relay = int(relay_number) -1,
        heater_on = relays["8"] == 0
    )

@app.route('/camera', methods=['GET', 'POST'])
def camera_view():
    recent = camera.recent()
    recent.reverse()
    recent = [{
        'date':r.split('_')[0].replace('-', '/'),
        'time':r.split('_')[1].split('.')[0].replace('-', ':'),
        'filename':r
    } for r in recent]
    return render_template(
        'camera.html',
        recent = recent
    )

@app.route('/camera/capture', methods=['GET', 'POST'])
def capture():
    response = camera.capture_still()
    print(f'DEBUG : {response}')
    return jsonify(response)

@app.route('/camera/latest', methods=['GET', 'POST'])
def latest_capture():
    response = camera.recent()
    return jsonify(response[-1])

@app.route('/light', methods=['GET', 'POST'])
def automate():
    if request.method == 'POST':
        if 'schedule' in request.form:
            new_schedule = [[r.split('|')[0], r.split('|')[1]] for r in request.form['schedule'].split(',') if '|' in r]
            schedule = set_schedule(new_schedule)
    else:
        schedule = read_schedule()

    schedule_today = [s for s in schedule if s[0] == datetime.now().strftime('%Y-%m-%d')]
    if schedule_today == []:
        schedule_today = ['', '']
    else:
        schedule_today = schedule_today[0]
    schedule_summary = [schedule[0], schedule_today, schedule[-1]]
    return render_template(
        'light.html',
        summary = schedule_summary,
        chart_url = schedule_charts(schedule, schedule_today)
    )

# === Back end ===
def set_schedule(schedule):
    full_schedule = []
    for x in range(len(schedule)):
        if x < len(schedule) -1:
            start = datetime.strptime(schedule[x][0], '%Y-%m-%d')
            stop = datetime.strptime(schedule[x +1][0], '%Y-%m-%d')
            difference = stop - start
            for i in range(difference.days):
                day = start + timedelta(days=i)
                full_schedule.append([day.strftime('%Y-%m-%d'), int(schedule[x][1])])
        else:
            full_schedule.append([schedule[x][0], int(schedule[x][1])])

    with open(f'{THIS_DIRECTORY}data{os.sep}light_schedule.json', 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(full_schedule, indent=4))

    return full_schedule

def read_schedule():
    schedule_file = f'{THIS_DIRECTORY}data{os.sep}light_schedule.json'
    if os.path.exists(schedule_file):
        with open(schedule_file, 'r', encoding='utf-8') as f:
            full_schedule = json.loads(f.read())
    else:
        today = datetime.now().strftime('%Y-%m-%d')
        full_schedule = [[today, 24]]
    return full_schedule

def schedule_charts(schedule, current):
    labels = [day[0].split('-')[-1] for day in schedule]
    label_string = '%7C'.join(labels)
    values = [day[1] for day in schedule]
    value_string = '%2C'.join([str(value) for value in values])
    if current[1] != '':
        current = f'{current[1]}%2C' * len(labels)
    chart = f'https://image-charts.com/chart?chco=FF0000%2CEBA870&chd=t%3A{current}%7C{value_string}&chdl=current%20%20%20%20%20%7Cscheduled&chdlp=t&chf=a%2Cs%2C00000000&chls=2%2C5%2C5%7C5&chs=800x300&cht=ls&chxl=1%3A%7C{label_string}&chxr=0%2C0%2C24&chxt=y%2Cx'
    return chart

def load_csv(filename, last_n=None):
    if os.path.exists(filename):
        with open(filename, 'r', encoding='utf-8') as f:
            reader = csv.reader(f)
            data = list(reader)
        if last_n is not None:
            data = data[0 - last_n:]
        data = [[d[0], round(float(d[1]), 1), round(float(d[2]), 1), d[3], d[4]] for d in data] # round values
        return data
    else:
        return [['2022-04-11 00:00', 0, 0]]

def environment_charts(log):
    charts = []
    for column in range(1, 3):
        all_values = [entry[column] for entry in log]
        labels = []
        values = []
        for index, timestamp in enumerate([entry[0] for entry in log]):
            if timestamp.split(':')[-1] == '00':
                labels.append(timestamp.split(' ')[-1])
                values.append(int(all_values[index]))
        labels = '%7C'.join(labels[::2])
        value_string = '%2C'.join([str(value) for value in values])
        if column == 1:
            colour = 'EBA870'
            target_upper = 24
            target_lower = 20
        else:
            colour = '70EBE7'
            target_upper = 60
            target_lower = 50
        target_upper = f'{target_upper}%2C' * len(values)
        target_lower = f'{target_lower}%2C' * len(values)
        charts.append(f'https://image-charts.com/chart?chf=a%2Cs%2C00000000&chd=t%3A{value_string}|{target_upper}|{target_lower}&chls=5|1,5,5|1,5,5&chs=800x300&cht=ls&chxl=1%3A%7C{labels}&chxr=0%2C{min(values) -5}%2C{max(values) +5}&chxt=y%2Cx&chco={colour}')
    return charts

if __name__ == '__main__':
    if platform.system() == 'Windows':
        debug = True
    else:
        debug = False
    app.run( host='0.0.0.0', port=5000, debug=debug )
