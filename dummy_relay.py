import os
import json

dirname, scriptname = os.path.split(os.path.abspath(__file__))
STATE_FILE = f'{dirname}{os.sep}data{os.sep}relay_state.json'

ON = 0
OFF = 1

PINS = {
    "1": 18, # heat lamp
    "2": 27, # heat lamp
    "3": 22, # heat lamp
    "4": 23, # heat lamp
    "5": 24, # heat lamp
    "6": 25, # heat lamp
    "7": 4,  # light
    "8": 17  # fan heater
}

def init():
    off(list(PINS))

def on(ports):
    # LOW signal = relay ON
    state_data = state()
    for port in ports:
        port = str(port)
        if port in PINS:
            state_data[port] = ON
    save_json(STATE_FILE, state_data)

def off(ports):
    # HIGH signal = relay OFF
    state_data = state()
    for port in ports:
        port = str(port)
        if port in PINS:
            state_data[port] = OFF
    save_json(STATE_FILE, state_data)

def state():
    '''read current state & invert, so True = ON'''
    if os.path.exists(STATE_FILE):
        state_data = load_json(STATE_FILE)
    else:
        state_data = {'1':OFF, '2':OFF, '3':OFF, '4':OFF, '5':OFF, '6':OFF, '7':OFF, '8':OFF}
    return state_data

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data
