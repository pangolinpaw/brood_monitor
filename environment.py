import time
import csv
import os
from datetime import datetime

import board
import busio
import adafruit_sht31d

import dimmer
import relay

i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_sht31d.SHT31D(i2c)

INTERVAL = 300 # time (seconds) between readings

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

def save_csv(filename, data):
    with open(filename, 'a', newline='', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(data)

def wait_to_start():
    '''wait until the time is divisible by 5 for tidy timestamps'''
    while True:
        minutes = datetime.now().strftime('%M')
        if int(minutes) % 5 == 0:
            break
        time.sleep(30)

def temperature():
    return sensor.temperature

def humidity():
    return sensor.relative_humidity

def main():
    if not os.path.exists(f'{THIS_DIRECTORY}data'):
        os.mkdir(f'{THIS_DIRECTORY}data')
    while True:
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M')
        relays = relay.state()
        for relay_number in relays:
            if int(relay_number) <= 6:
                if relays[relay_number] == 1:
                    break # Find last relay that's ON
            else:
                break
        heat_lamps_on = relays -1
        light_on = relays['7'] == 0
        heater_on = relays['8'] == 0
        readings = [timestamp, sensor.temperature, sensor.relative_humidity, heater_on, heat_lamps_on, light_on]
        print('\t'.join([str(r) for r in readings]))
        save_csv(f'{THIS_DIRECTORY}data{os.sep}environment_log.csv', readings)
        time.sleep(INTERVAL)

if __name__ == '__main__':
    wait_to_start()
    main()
