import os
import json

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

STATE_FILE = f'{THIS_DIRECTORY}data/state.json'

def init():
    off(1)
    record_state(1, 0)
    off(2)
    record_state(2, 0)

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def record_state(channel, angle):
    current_state = state()
    current_state[f'dimmer_{channel}'] = int(angle)
    save_json(STATE_FILE, current_state)

def state():
    if os.path.exists(STATE_FILE):
        current_state = load_json(STATE_FILE)
    else:
        current_state = {'dimmer_1':0, 'dimmer_2':0}
    return current_state

def set(channel, angle):
    print(f'DIMMER {channel} SET TO {angle}')
    record_state(channel, angle)
    return True

def on(arg):
    print(f'DIMMER ON: {arg}')
    return True

def off(arg):
    print(f'DIMMER OFF: {arg}')
    return True
