import os
import time
from datetime import datetime

INTERVAL = 300 # time (seconds) between captures

dirname, scriptname = os.path.split(os.path.abspath(__file__))
CAPTURE_DIR = f'{dirname}{os.sep}static{os.sep}captures'

def list_files(folder, extensions=None):
    file_list = []
    all_files = os.listdir(folder)
    for name in all_files:
        if extensions is not None:
            for ext in extensions:
                if name.endswith(ext):
                    file_list.append(f'{folder}{os.sep}{name}')
        else:
            file_list.append(f'{folder}{os.sep}{name}')
    return file_list

def capture_still():
    timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M')
    os.system(f'fswebcam -r 800x600 --jpeg 85 -D 1 {timestamp}.jpg')
    return f'{timestamp}.jpg'

def recent():
    stills = list_files(CAPTURE_DIR, ['png', 'jpg'])
    to_delete = stills[10:]
    for still in to_delete:
        os.remove(still)
    return [s.split(os.sep)[-1] for s in stills[:10]]

def wait_to_start():
    while True:
        minutes = datetime.now().strftime('%M')
        if int(minutes) % 5 == 0:
            break
        time.sleep(30)

def main():
    while True:
        capture_still()
        time.sleep(INTERVAL)

if __name__ == '__main__':
    wait_to_start()
    main()
