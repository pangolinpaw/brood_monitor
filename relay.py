import time
import json
import os
from datetime import datetime, timedelta
import RPi.GPIO as GPIO

dirname, scriptname = os.path.split(os.path.abspath(__file__))
STATE_FILE = f'{dirname}{os.sep}data{os.sep}relay_state.json'
SCHEDULE_FILE = f'{dirname}data{os.sep}light_schedule.json'

ON = 0
OFF = 1

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

PINS = {
    "1": 18, # heat lamp
    "2": 27, # heat lamp
    "3": 22, # heat lamp
    "4": 23, # heat lamp
    "5": 24, # heat lamp
    "6": 25, # heat lamp
    "7": 4,  # light
    "8": 17  # fan heater
}

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def init():
    for pin in PINS:
        GPIO.setup(PINS[pin], GPIO.OUT)
    off(list(PINS))

def on(ports):
    # LOW signal = relay ON
    state_data = state()
    for port in ports:
        port = str(port)
        if port in PINS:
            GPIO.output(PINS[port], ON)
            state_data[port] = ON
    save_json(STATE_FILE, state_data)

def off(ports):
    # HIGH signal = relay OFF
    state_data = state()
    for port in ports:
        port = str(port)
        if port in PINS:
            GPIO.output(PINS[port], OFF)
            state_data[port] = OFF
    save_json(STATE_FILE, state_data)

def state():
    '''read current state & invert, so True = ON'''
    if os.path.exists(STATE_FILE):
        state_data = load_json(STATE_FILE)
    else:
        state_data = {'1':OFF, '2':OFF, '3':OFF, '4':OFF, '5':OFF, '6':OFF, '7':OFF, '8':OFF}
    return state_data

def scheduled_lighting():
    if os.path.exists(SCHEDULE_FILE):
        full_schedule = load_json(SCHEDULE_FILE)
        schedule = [s for s in schedule if s[0] == datetime.now().strftime('%Y-%m-%d')]
        if schedule != []:
            hours = int(schedule[1])
            current_time = datetime.now()
            on_time = datetime.strptime(f'{current_time.strftime("%Y-%m-%d ")} 12:00', '%Y-%m-%d %H:%M') - timedelta(hours=round(hours / 2, 1))
            off_time = datetime.strptime(f'{current_time.strftime("%Y-%m-%d ")} 12:00', '%Y-%m-%d %H:%M') + timedelta(hours=round(hours / 2, 1))
            while on_time < current_time < off_time:
                on(["7"])
                time.sleep(300) # check every 5 min (300 sec)
                current_time = datetime.now()
    off(["7"])


def test():
    try:
        GPIO.output(PINS[3], 0)
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        GPIO.output(PINS[3], 1)


if __name__ == '__main__':
    init()
    scheduled_lighting()
