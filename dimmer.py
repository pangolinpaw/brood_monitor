#!/usr/bin/python
# -*- coding:utf-8 -*-
import os
import sys
import time
import json
from waveshare_2_CH_SCR_HAL import SCR
scr = SCR.SCR(data_mode = 0) # 0:I2C  1: UART

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

STATE_FILE = f'{THIS_DIRECTORY}data/dimmer_state.json'

def init():
    off(1)
    record_state(1, 0)
    off(2)
    record_state(2, 0)

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def record_state(channel, angle):
    current_state = state()
    current_state[f'dimmer_{channel}'] = int(angle)
    save_json(STATE_FILE, current_state)

def state():
    if os.path.exists(STATE_FILE):
        current_state = load_json(STATE_FILE)
    else:
        current_state = {'dimmer_1':0, 'dimmer_2':0}
    return current_state

def set(channel, angle):
    scr.SetMode(1)
    scr.VoltageRegulation(channel, 0)
    scr.ChannelEnable(channel)
    if (angle * 2) < 180:
        scr.VoltageRegulation(channel, (angle * 2) % 180)
    else:
        if angle > 180:
            angle = 180
        scr.VoltageRegulation(channel, 180 - (angle * 2) % 180)
    record_state(channel, angle)

def on(channel):
    scr.SetMode(1)
    scr.VoltageRegulation(channel,0)
    scr.ChannelEnable(channel)
    for angle in range(0, 180):
        if (angle * 2) < 180:
            scr.VoltageRegulation(channel, (angle * 2) % 180)
        else:
            scr.VoltageRegulation(channel, 180 - (angle * 2) % 180)
        time.sleep(0.1)

def off(channel):
    scr.ChannelDisable(channel)
    scr.VoltageRegulation(channel, 0)

def demo():
    try:
        for x in range(3):
            print(' Channel 1 ON')
            on(1)
            print(' Channel 2 ON')
            on(2)
            time.sleep(0.2)
            print(' All OFF')
            off(1)
            off(2)
            time.sleep(2)
    except KeyboardInterrupt:
        print(' All OFF (interrupted)')
        off(1)
        off(2)

if __name__ == '__main__':
    demo()
