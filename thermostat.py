import time
import platform

import json
import os
import random

if platform.system() == 'Windows':
    import dummy_relay as relay
else:
    import relay


def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def current_temp():
    # live system will read this from environment.temperature()
    test_file = 'data/testing.json'
    if os.path.exists(test_file):
        previous = load_json(test_file)
    else:
        previous = random.randint(15, 20)
    current = previous + ( random.randint(1, 3) * random.choice([-1, 1, 1]) )
    save_json(test_file, current)
    return current

def maintain(target=30, power='medium', buffer=0):
    power_levels = {
        'low':4,
        'medium':2,
        'high':1
    }
    if power in power_levels:
        power = power_levels[power]
    else:
        power = 2
    while True:
        temperature = current_temp()
        difference = target - temperature
        print(f'current: {temperature}, difference: {difference}')

        if difference <= buffer:
            # all off
            relay.off(list(range(1, 9)))
        else:
            max_relay = int( round((difference / 2), 0) )
            max_relay = min(max_relay, 8) # max 8 heat lamps
            relay.off(list(range(1, 9)))
            relay.on( list(range(1, max_relay +1 ) ) )

        print()
        time.sleep(2)

if __name__ == '__main__':
    maintain(power='high')
