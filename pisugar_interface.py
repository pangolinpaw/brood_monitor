import subprocess

def battery_level():
    proc = subprocess.Popen('echo "get battery" | nc -q 0 127.0.0.1 8423', shell=True, stdout=subprocess.PIPE)
    output = proc.stdout.read().decode('utf-8')
    if ':' in output:
        return int(round(float(output.split(':')[-1].strip()), 0))

def shutdown_level():
    proc = subprocess.Popen('echo "get safe_shutdown_level" | nc -q 0 127.0.0.1 8423', shell=True, stdout=subprocess.PIPE)
    output = proc.stdout.read().decode('utf-8')
    if ':' in output:
        return int(output.split(':')[-1].strip())

def battery_charging():
    proc = subprocess.Popen('echo "get battery_charging" | nc -q 0 127.0.0.1 8423', shell=True, stdout=subprocess.PIPE)
    output = proc.stdout.read().decode('utf-8')
    if ':' in output:
        return output.split(':')[-1].strip() == 'true'

def wake_up_time():
    proc = subprocess.Popen('echo "get rtc_alarm_time" | nc -q 0 127.0.0.1 8423', shell=True, stdout=subprocess.PIPE)
    output = proc.stdout.read().decode('utf-8')
    if ':' in output:
        return output.split(': ')[-1].strip()

def current_time():
    proc = subprocess.Popen('echo "get rtc_time" | nc -q 0 127.0.0.1 8423', shell=True, stdout=subprocess.PIPE)
    output = proc.stdout.read().decode('utf-8')
    if ':' in output:
        return output.split(': ')[-1].strip()

if __name__ == '__main__':
    print(f'battery_level: {battery_level()}')
    print(f'battery_charging: {battery_charging()}')
    print(f'shutdown_level: {shutdown_level()}')
    print(f'wake_up_time: {wake_up_time()}')
    print(f'current_time: {current_time()}')
